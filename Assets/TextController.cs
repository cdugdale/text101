﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour 
{

	public Text text;
	private enum States {
		cell, mirror, sheets_0, lock_0, cell_mirror, sheets_1, lock_1, corridor_0, corridor_1, corridor_2, vent_0, vent_1, 
		stairs_0, stairs_1, stairs_2, stairs_3, floor, cntrlRm_0, cntrlRm_1, vent_2, stairs_4, courtyard, maingate, freedom
	};
	private States myState;

	// Use this for initialization
	void Start ()
	{
		myState = States.cell;
		print ("state set to cell");
	}

	// Update is called once per frame
	void Update ()
	{
		print (myState);
		if (myState == States.cell) 				{ Cell (); }
		else if (myState == States.sheets_0) 		{ Sheets_0(); } 
		else if (myState == States.mirror) 			{ Mirror(); } 
		else if (myState == States.lock_0) 			{ Lock_0(); } 
		else if (myState == States.sheets_1) 		{ Sheets_1(); } 
		else if (myState == States.cell_mirror) 	{ Cell_Mirror(); } 
		else if (myState == States.lock_1) 			{ Lock_1(); } 
		else if (myState == States.corridor_0) 		{ Corridor_0();}
		else if (myState == States.corridor_1) 		{ Corridor_1(); } 
		else if (myState == States.corridor_2) 		{ Corridor_2(); } 
		//else if (myState == States.corridor_2) 		{ Corridor_3(); } 
		else if (myState == States.vent_0) 			{ Vent_0(); } 
		else if (myState == States.vent_1) 			{ Vent_1(); } 
		else if (myState == States.stairs_0) 		{ Stairs_0(); } 
		else if (myState == States.stairs_1) 		{ Stairs_1(); } 
		else if (myState == States.stairs_2) 		{ Stairs_2(); } 
//		else if (myState == States.stairs_3) 		{ Stairs_3(); } 
		else if (myState == States.floor) 			{ Floor(); } 
		else if (myState == States.cntrlRm_0) 		{ CntrlRm_0(); } 
		else if (myState == States.cntrlRm_1) 		{ CntrlRm_1(); } 
		else if (myState == States.vent_2)			{ Vent_2(); } 
		//else if (myState == States.stairs_4) 		{ Stairs_4(); } 
		else if (myState == States.courtyard) 		{ Courtyard(); } 
		else if (myState == States.maingate) 		{ Maingate(); }
		else if (myState == States.freedom) 		{ Freedom(); } 
	}

	void Cell ()
	{
	print("cell called ");
		text.text = "You are in a prison cell, number 14, block D, and you really don't want to be here any more, there are some dirty sheets on the bed, " +
					"a mirror on the wall, and the door is locked from the outside.\n\n" +
					"Press S to view Sheets, M to view Mirror and L to view Lock";
		if 		(Input.GetKeyDown (KeyCode.S)) 	{ myState = States.sheets_0; }
		else if (Input.GetKeyDown (KeyCode.M)) 	{ myState = States.mirror; }
		else if (Input.GetKeyDown (KeyCode.L)) 	{ myState = States.lock_0; }
	}

	void Sheets_0 ()
	{
		text.text = "you look at the sheets, they're functional, but not pretty. Prison funding doesn't stretch to Laura Ashley apparently. " +
					"You put them back on the bed. \n\n" +
					"press R to return to roaming the cell";
		
		if (Input.GetKeyDown (KeyCode.R)) { myState = States.cell; }
			
	}

	void Mirror ()
	{
		text.text = "you look at the mirror and admire your dirt stained orange jump suit. " +
					"after telling yourself that this is only a minor set back, you notice that " +
					"the mirror is loose.\n\n" +
					"press R to return to roaming the cell, or T to take the mirror.";
		
		if 		(Input.GetKeyDown (KeyCode.R)) 	{ myState = States.cell; }
		else if (Input.GetKeyDown (KeyCode.T)) 	{ myState = States.cell_mirror; }
			
	}

	void Lock_0 ()
	{
		text.text = "you look at the lock, it's locked tight. It's not a key lock, it's a digital lock with a number pad.\n\n" +
					"Press R to return to roaming the cell";
		
		if (Input.GetKeyDown (KeyCode.R)) { myState = States.cell; }
			
	}

	void Sheets_1 ()
	{
		text.text = "you look at the sheets woth the mirror, that hasn't helped at all, now they're not pretty and reveresed, so you put them down \n\n" +
					"press R to return to roaming the cell";
		
		if (Input.GetKeyDown (KeyCode.R)) { myState = States.cell_mirror; }
			
	}

	void Cell_Mirror ()
	{
		text.text = "You have the mirror and look around the cell to try to see what you can with it. " +
					"Not much else has changed in the cell \n\n" +
					"Press S to look at the sheets, and press L to look at the lock";

		if 		(Input.GetKeyDown (KeyCode.S)) 	{ myState = States.sheets_1; }
		else if (Input.GetKeyDown (KeyCode.L)) 	{ myState = States.lock_1; }
	}

	void Lock_1 ()
	{
		text.text = "You look at the lock again, and realise that if you put the mirror through the bars" +
					"and angle it just so..... you can see that some of the buttons on the keypad are a little worn and " +
					"you might just be able to work out which ones are pressed most" +
					"\n\n"  +
					"press R to return to roaming the cell, press O to try to open the lock";
		
		if (Input.GetKeyDown (KeyCode.R)) 		{ myState = States.cell_mirror; }
		else if (Input.GetKeyDown (KeyCode.O)) 	{ myState = States.corridor_0; }
	}

	void Corridor_0 ()
	{
		text.text = "You did it, freedom is yours! \n\n" +
					"Oh hang on. That's not good. You're just in a prison corridor. Of course you are! like it would be that easy, that would be a " +
					"rubbish prison, what were you thinking? \n\n" +
					"Right, what next? You can see that at the end of the corridor are some stairs, there's a vent near the floor to your right, " +
					"and it looks like there's something shiny on the floor"+
					"\n\nPress S to go up the stairs, F to look at the floor and V to look at the vent ";
		
		if (Input.GetKeyDown (KeyCode.S)) 		{ myState = States.stairs_0; } 
		else if (Input.GetKeyDown (KeyCode.F)) 	{ myState = States.floor; } 
		else if (Input.GetKeyDown (KeyCode.V)) 	{ myState = States.vent_0; } 
	}

	void Stairs_0 ()
	{
		text.text = "You walk to the bottom of the stairs and you can hear voices, very carefully you inch up the stairs " + 
					"and see that there is a guards station with another locked door just past it. You don't have a weapon, "+
					"and to be honest you think you wouldn't really know what to do with one if you did, so that looks like a no go" +
					"\n\n"  +
					"press R to return to the corridor";
		
		if (Input.GetKeyDown (KeyCode.R)) 		{ myState = States.corridor_0; }
	}

	void Floor ()
	{
		text.text = "It's a spoon, not a knife, not a gun, not a guards keycard, it's a spoon. Great, just your luck! " + 
					"\n\n"  +
					"press S to pick up the spoon, press R to return to the corridor";
		
		if (Input.GetKeyDown (KeyCode.R)) 		{ myState = States.corridor_0; }
		else if (Input.GetKeyDown (KeyCode.S)) 	{ myState = States.corridor_1; }
	}

	void Vent_0 ()
	{
		text.text = "Hmmm, a vent, not a big one, maybe an air conditioning outlet. You think you might be able to get into it with a bit of a squeeze." + 
					"If only you hadn't had those extra cakes it would be easier. There are 4 screws holding the vent on, so unless you can find something to unscrew them with"  +
					"then that's no use anyway"  +
					"\n\n"  +
					"Press R to return to the corridor";
		
		if (Input.GetKeyDown (KeyCode.R)) 		{ myState = States.corridor_0; }
	}

	void Corridor_1 ()
	{
		text.text = "You look aroud the corridor again, but keeping the spooon in mind, what can you do now?" +
					"\n\n" +
					"Press S to go up the stairs and V to look at the vent ";
		
		if (Input.GetKeyDown (KeyCode.S)) 		{ myState = States.stairs_1; } 
		else if (Input.GetKeyDown (KeyCode.V)) 	{ myState = States.vent_1; } 
	}

	void Stairs_1 ()
	{
		text.text = "You walk to the bottom of the stairs and you can hear those voices again, at least you have a weapon now. " + 
					"Oh no, that's right, you only have a spoon. You feel as if spoon versus armed guards isn't something that's going to go in your favour."+
					"\n\n"  +
					"press R to return to the corridor";
		
		if (Input.GetKeyDown (KeyCode.R)) 		{ myState = States.corridor_1; }
	}

	void Vent_1 ()
	{
		text.text = "When you look closer you can see that the screws are flathead ones, the spoon might actually be useful! " +
					"You unscrew the screws and get in, ok, squeeze in to the vent. You can see a room at the other end that sounds like it has a lot of computer" +
					"eqiupment in. "  +
					"\n\n"  +
					"Press C to go into the computer room";
		
		if (Input.GetKeyDown (KeyCode.C)) 		{ myState = States.cntrlRm_0; }
	}

	void CntrlRm_0 ()
	{
		text.text = "You look around from inside the vent and don't see anyone at the computers. You decide you may as well get out and see if you "+
		"can find anything that will help you escape. You see a load of buttons on the compter desks, having read them you can see that one says open " + 
		"Block D outer door. That's surely worth a try? " +
		"\n\n" +
		"Press V to go back to the vent, press o to open up the block d outer door then head back to the vent";
		if (Input.GetKeyDown (KeyCode.V)) 		{ myState = States.vent_1; } 
		else if (Input.GetKeyDown (KeyCode.O)) 	{ myState = States.vent_2; } 
	}

	void Vent_2 ()
	{
		text.text = "You are back in the vent and the blcok d door is open, you can either go back to the control room, or go to the corridor where you came from "  +
		"\n\n"  +
		"Press C to go into the corridor, R to return to the computer room";
		
		if (Input.GetKeyDown (KeyCode.C)) 		{ myState = States.corridor_2; }
		else if (Input.GetKeyDown (KeyCode.R)) 	{ myState = States.cntrlRm_1; }
	}

	void CntrlRm_1 ()
	{
		text.text = "Theres nothing left here to look at, you may as well leave." +
		"\n\n" +
		"Press V to go back to the vent";
		if (Input.GetKeyDown (KeyCode.V)) 		{ myState = States.vent_2; } 
	}


	void Corridor_2 ()
	{
		text.text = "You are in the corridor with the unscrewed vent, the block door is open and the stairs are just in front of you. and the stairs" +
					"\n\n" +
					"Press S to go up the stairs and V to look at the vent ";
		
		if (Input.GetKeyDown (KeyCode.V)) 		{ myState = States.vent_2; } 
		else if (Input.GetKeyDown (KeyCode.S)) 	{ myState = States.stairs_2; } 
	}

	void Stairs_2 ()
	{
		text.text = "You creep up the stairs again and this time you can't hear any voices, maybe the guards have gone. You poke your head round the top of the stairs " +
					"and just passed the guards station you can see the door is now open" +
					"\n\n" +
					"Press R to return to the corridor, press C to go out into the courtyard";
		if (Input.GetKeyDown (KeyCode.R)) 		{ myState = States.corridor_2; } 
		else if (Input.GetKeyDown (KeyCode.C)) 	{ myState = States.courtyard; } 
	}

	void Courtyard ()
	{
		text.text = "The courtyard is deserted! The door being opened must have sent the guards inside to " +
					"find out what happened. There are only 2 things you can do now, go towards the main gate and hope for the best, or go back to the stairs where you came from" +
					"\n\n" +
					"Press S to return to the Stairs, press M to go towards the main gate";
		if (Input.GetKeyDown (KeyCode.S)) 		{ myState = States.stairs_3; } 
		else if (Input.GetKeyDown (KeyCode.M)) 	{ myState = States.maingate; } 
	}

	void Maingate ()
	{
		text.text = "This is it, you are approaching the main gate. There's a van leaving, if you time it jsut right you can get alongside it on the "+
					"opposite side to the guard post and you might just make it out" +
					"\n\n" +
					"Press V to follow alongside the van, press C to go back to the courtyard";
		if (Input.GetKeyDown (KeyCode.V)) 		{ myState = States.freedom; } 
		else if (Input.GetKeyDown (KeyCode.C)) 	{ myState = States.courtyard; } 
	}

	void Freedom ()
	{
		text.text = "It's worked! You've made it out past the guards by tracking alongside the van! "+
					"Now all you have to do is ditch that orange jump suit and get some otehr clothes, but that will be simple, right?...." +
					"\n\n" +
					"Press P to play again";
		if (Input.GetKeyDown (KeyCode.P)) 		{ myState = States.cell; } 
	}

}